package q4blocks.service.ws;

public class ClientMessage {

    private String type;
    private String body;
    private Invocation invocation;

	public ClientMessage() {
    }

	public ClientMessage(String type) {
        this.type = type;
    }

    public String getBody() {
		return body;
	}

	public Invocation getInvocation() {
		return invocation;
	}

	public String getType() {
        return type;
    }

    public void setBody(String body) {
		this.body = body;
	}

    public void setInvocation(Invocation invocation) {
		this.invocation = invocation;
	}

    public void setType(String type) {
        this.type = type;
    }
}
