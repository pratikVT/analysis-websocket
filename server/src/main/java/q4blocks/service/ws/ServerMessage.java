package q4blocks.service.ws;

import java.util.List;

import analysis.transform.Action;

public class ServerMessage {

    private String type;
    private List<Action> body;

    public List<Action> getBody() {
		return body;
	}

	public void setBody(List<Action> body) {
		this.body = body;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ServerMessage() {
    }

    public ServerMessage(String type) {
        this.type = type;
    }
}
