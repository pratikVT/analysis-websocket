package q4blocks.service.ws;

public class Invocation {
	private String refactoring;
	private String blockId;
	private String blocks;
	

	public String getBlockId() {
		return blockId;
	}

	public String getBlocks() {
		return blocks;
	}

	public String getRefactoring() {
		return refactoring;
	}

	public void setBlockId(String blockId) {
		this.blockId = blockId;
	}

	public void setBlocks(String blocks) {
		this.blocks = blocks;
	}

	public void setRefactoring(String refactoring) {
		this.refactoring = refactoring;
	}

	@Override
	public String toString() {
		return "Invocation [refactoring=" + refactoring + ", blockId=" + blockId + ", blocks=" + blocks + "]";
	}
	
}
