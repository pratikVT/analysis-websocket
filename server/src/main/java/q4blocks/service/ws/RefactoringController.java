package q4blocks.service.ws;

import java.util.List;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import analysis.transform.Action;
import analysis.transform.ExtractVariable;

@Controller
public class RefactoringController {

    @MessageMapping("/request") // message destination
    @SendToUser // for peer-to-peer communication
    public ServerMessage clientRequestHandler(ClientMessage clientMsg) throws Exception {
    	// test connection
    	if(clientMsg.getType().equals("test")) {
    		ServerMessage serverMsg = new ServerMessage();
    		serverMsg.setType("test");
            return serverMsg;
    	}
    	
    	// invocation
    	ServerMessage serverMsg = new ServerMessage();
		Invocation inv = clientMsg.getInvocation();
		
		if(clientMsg.getInvocation().getRefactoring().equals("extract_var")) {
			List<Action> actions = ExtractVariable.perform("", inv.getBlockId(), inv.getBlocks());			
			serverMsg.setBody(actions);
		}
		
		serverMsg.setType("transformation");
		
        return serverMsg;
    }

}
