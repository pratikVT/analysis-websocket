Sprint framework based WebSocket
================================

based on https://spring.io/guides/gs/messaging-stomp-websocket/

Require:
smellanalyzer-1.jar in server/src/main/libs

## How to run:

* run without building a jar:

```
cd server
./gradlew bootRun
```

* compile and run jar

```
cd server
./gradlew build
java -jar build/libs/gs-messaging-stomp-websocket-0.1.0.jar
```


## Run with Docker
* build the image
```
docker build -t websocket-app .
```

* run a container
```
docker run -it -p 8080:8080 websocket-app
```




## Miscellaneous
* Importing Gradle project into Eclipse
Import... Gradle->Existing Gradle Project

* Gradle tasks can be accessed in Gradle Tasks view
If not, Windows -> Show View... -> Other -> Gradle -> Gradle Tasks

* Sometimes the application does not shutdown properly, preventing future rerun because of port is already in use.
To fix, kill the process running on port 8080:
`fuser -k 8080/tcp`
